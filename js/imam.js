function seeAchievement() {
  window.scroll({
    top: 640,
    behavior: 'smooth'
  });
}

$(document).ready(function(){
  $('#support').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [{
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
});
